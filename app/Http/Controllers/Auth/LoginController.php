<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Rules\ExpireSmsCode;
use App\Rules\ValidationSmsCode;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Arr;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showConfirmSms($token)
    {
//        dd($token);
//        $this->validate($request,[
//            'sms_code'=>'numeric'
//        ]);
        $phone = $token;
        return view('auth.confirm.sms', compact('phone'));
    }

    public function confirmSms(Request $request, $token)
    {
        $re = '/\{(?:[^{}]|(?R))*\}/';
        $str = base64_decode($token);
        preg_match_all($re, $str, $matches);
        $matches = Arr::flatten($matches);
        foreach ($matches as $res ) {
            $obj = json_decode($res);
            if (isset($obj->{'phone'})) {
                echo $obj->{'phone'};
                echo "<br>" ;
            }
        }


        //        check if sms code correct
//        dd($request->sms_code);
//        $request->validate([
//            'phone'=> ['require'],
//            'sms_code' => [
//                new ValidationSmsCode($request->phone,$request->sms_code),
//            ]
//        ]);

    }


}
