<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Rules\EmailOrPhone;
use App\Rules\ExpireSmsCode;
use App\Rules\ValidationSmsCode;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Auth\Events\Registered;
use Kavenegar;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

//    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['guest', 'throttle:30,1']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function register(Request $request)
    {
        $validated = $request->validate([
            'phone_email' => [
                'required',
                new EmailOrPhone($request->phone_email),
            ],
        ]);

        if ($this->checkEmailOrPhone($request->phone_email) == 'email') {
            $validated = $request->validate([
                'phone_email' => [
                    Rule::exists('users', 'email')
                ]
            ]);

            return view('auth.login');
        }

        if ($this->checkEmailOrPhone($request->phone_email) == 'phone') {
            $user_exist = User::where('phone_number', $request->phone_email)->first();
            if (!$user_exist) {
                User::create([
                    'phone_number' => $request->phone_email,
                ]);
            }

            $request->validate([
                'phone_email' => [
                    new ExpireSmsCode($request->phone_email),
                ]
            ]);

            $sms_code = $this->generateSmsCode();
//            if($this->sendSms("09381314549", $sms_code, "message ")){
            $user = User::updateOrCreate([
                'phone_number' => $request->phone_email,
            ], [
                'sms_code' => $sms_code,
                'sms_expired_at' => Carbon::now()->addMinute(2),
            ]);
//            } else {
//                echo "مشکل در ارسال اس ام اس ، لطفا بعدا اقدام کنید." ;
//            }
            $user = $user->findOrFail($user->id);

//            return view('auth.confirm.sms');
//            dd($request->phone_email);
            $phone_number = $request->phone_email;
//            return redirect()->route('showConfirm.sms')->with('phone_number',$phone_number);
            $token = $this->generateIdentifierPhoneToken($phone_number) ;
            return Redirect::to('/confirm/sms/'.$token);
//            return view('auth.confirm.sms')->with('phone_number', $phone_number);

        }

    }

    public function generateIdentifierPhoneToken ($phone) {
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

// Create token payload as a JSON string
        $payload = json_encode(['phone' => $phone]);

// Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

// Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

// Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'abC123!', true);

// Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

// Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
        return $jwt ;
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    public function checkEmailOrPhone($request)
    {
        if (is_numeric($request)) {
            return 'phone';
        }
        if (filter_var($request, FILTER_VALIDATE_EMAIL)) {
            return 'email';
        }
    }

    public function generateSmsCode()
    {
        $faker = \Faker\Factory::create();
        return $faker->randomNumber(6, true);
    }

    public function sendSms($receptor, $code, $message)
    {
        try {
            $api = new \Kavenegar\KavenegarApi("7136576F3256554C4C686336633543373768394263326B30746F6242313862766C2B35737044474D5A45593D");
            $sender = "1000596446";
            // $message = "";
            // $receptor = array("09117661876","09123356784");
            $messageContent = $message . "  " . $code;
            $result = $api->Send($sender, $receptor, $messageContent);
            if ($result) {
//                 foreach($result as $r){
//                    echo "messageid = $r->messageid";
//                    echo "message = $r->message";
//                    echo "status = $r->status";
//                    echo "statustext = $r->statustext";
//                    echo "sender = $r->sender";
//                    echo "receptor = $r->receptor";
//                    echo "date = $r->date";
//                    echo "cost = $r->cost";
//                }
                return true;
            }
        } catch (\Kavenegar\Exceptions\ApiException $e) {
            // echo $e->errorMessage();
            return false;
            // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد

        } catch (\Kavenegar\Exceptions\HttpException $e) {
            // echo $e->errorMessage();
            return false;
            // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد

        }
    }
}
