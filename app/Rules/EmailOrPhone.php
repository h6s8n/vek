<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;


class EmailOrPhone implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $phone_email;

    public function __construct($phone_email)
    {
        $this->phone_email = $phone_email;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */


    public function passes($attribute, $value)
    {

        if (is_numeric($this->phone_email) && strlen($this->phone_email) == 11) {
            return true;
        }

        if (filter_var($this->phone_email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'email or phone number invalid';
    }
}
