<?php

namespace App\Rules;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class ValidationSmsCode implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $phone ;
    protected $code ;

    public function __construct($phone,$code)
    {
        $this->phone = $phone ;
        $this->code = $code ;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
//        $user = User::where('phone_number', $this->phone)->where("sms_code",$this->code)->first();
//        if ($user){
//            return true ;
//        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'sms code is invalid.';
    }
}
