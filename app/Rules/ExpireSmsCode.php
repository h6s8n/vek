<?php

namespace App\Rules;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class ExpireSmsCode implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $phone;

    public function __construct($phone_email)
    {
        $this->phone = $phone_email;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $now = Carbon::now();
        $user = User::where('phone_number', $this->phone)->whereRaw("(sms_expired_at is null or sms_expired_at < '$now')")->first();
        if ($user) return true ;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'sms code was sent for this phone plz try a few minute later.';
    }
}
