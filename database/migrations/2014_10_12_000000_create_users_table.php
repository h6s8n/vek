<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('last_name')->nullable();
            $table->bigInteger('national_code')->nullable();
            $table->bigInteger('phone_number')->unique();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('birth_date')->nullable();
            $table->string('job')->nullable();
            $table->string('password')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('home_plate')->nullable();
            $table->smallInteger('unit')->nullable();
            $table->integer('postal_code')->nullable();
            $table->string('sms_code')->nullable();
            $table->timestamp('sms_verified')->nullable();
            $table->timestamp('sms_expired_at')->nullable();
            $table->tinyInteger('depth')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
